from keras.utils import Sequence
import random
import math
import os
import re
import numpy as np
from random import randint

from generator.current_mode import ProcessingMode


class DataSequence(Sequence):
    """
    A customary sequence class to help obtain the input necessary for training.
    This one IGNORES temporal dimension, IGNORE spatial dimension.
    This one is customized to read the weather NPY data from the weather network challenge.
    """

    def __init__(
        self,
        date_txt_path: str = r"C:\GitHub\MarkerTrainer\data\date.txt",
        batch_size: int = 1048576,
        mode=ProcessingMode.Train,
    ):

        # read the csv file with pandas
        # self.df = pd.read_csv(date_txt_path)
        path_folder = os.path.dirname(date_txt_path)

        # Read the date.txt which has the information about the starting time of all the actual PNY files.
        self.file = open(date_txt_path, "r")
        self.file_lines = self.file.readlines()

        # FILE loop > Temporal Loop > Spatial Loop

        # How many files to concatenate, there are 53! and we best not to train on one file as that is limited to 3 months period.

        list_random_files = []

        # Few files, load them all.
        if len(self.file_lines) < 5:
            for file in self.file_lines:
                list_random_files.append(file)

        # otherwise, load way more.
        else:
            num_files = 3
            for x in range(num_files):
                # Generate
                chosen_file_index = randint(0, len(self.file_lines) - 1)
                chosen_file = self.file_lines[chosen_file_index]
                list_random_files.append(chosen_file)

        # batch size
        self.batch_size = batch_size

        # shuffle when in train mode
        self.mode = mode

        # Ground Truth Temperature from X files across N timepoints, across all possible X Y coordinates.
        self.nparray_ground_truth: np.ndarray = None
        # self.labels = self.df[['r1']].values  # THIS IS Y

        # List of variables from X files across N timepoints, across all possible X Y coordinates.
        self.nparray_input_variables_15: np.ndarray = None

        # Mode specific text prefix:
        if self.mode == ProcessingMode.Train:
            regex_file_name = re.compile(r"training_\d\d\d\d_\d\d\d\d.npy")
        elif self.mode == ProcessingMode.Validation:
            regex_file_name = re.compile(r"validation_\d\d\d\d_\d\d\d\d.npy")
        else:
            regex_file_name = re.compile(r"")
        regex_datetime = re.compile(r"201\d-\d\d-\d\d \d\d:\d\d:\d\d")

        # Loop through each file.
        for file_specification in list_random_files:

            if file_specification == "\n":
                continue

            print(file_specification)

            file_name = regex_file_name.findall(file_specification)[0]
            date_time = regex_datetime.findall(file_specification)[0]

            # hour_of_the_year = date_to_nth_hour(date_time)
            # print(file_name)

            # x should have 100 x 15 x 256 x 256
            x = np.load(os.path.join(path_folder, "input_" + file_name))

            # x_transposed should have 256 by 256 by 100 by 15
            x_transposed = x.transpose([2, 3, 0, 1])
            # Keeping the LAST dimension after the rearrangement:

            # x_flatten should now be 6553600, 15
            x_flatten = x_transposed.reshape(-1, x_transposed.shape[-1])

            # Concatenate this to the object level list
            if self.nparray_input_variables_15 is None:
                self.nparray_input_variables_15 = x_flatten
            else:
                self.nparray_input_variables_15 = np.concatenate(
                    (self.nparray_input_variables_15, x_flatten)
                )

            # y should have 100 x 1 x 256 x 256
            y = np.load(os.path.join(path_folder, "label_" + file_name))

            # y_transposed should have 256 by 256 by 100 by 1
            y_transposed = y.transpose([2, 3, 0, 1])

            # y_flatten should now be 6553600, 1
            y_flatten = y_transposed.reshape(-1, y_transposed.shape[-1])

            # Concatenate this to the object level list
            if self.nparray_ground_truth is None:
                self.nparray_ground_truth = y_flatten
            else:
                self.nparray_ground_truth = np.concatenate(
                    (self.nparray_ground_truth, y_flatten)
                )

        # Now we loaded the X number of files and then concatenated them together so we can have better representation of the weather data.

        # x_model1 should now be 6553600, 15

    def __len__(self):
        # compute number of batches to yield
        return int(
            math.ceil(self.nparray_input_variables_15.shape[0] / float(self.batch_size))
        )

    def on_epoch_end(self):
        # Shuffles indexes after each epoch if in training mode
        self.indexes = range(len(self.nparray_input_variables_15))
        if self.mode == "train":
            self.indexes = random.sample(self.indexes, k=len(self.indexes))

    def get_batch_labels(self, index_batch):
        """
        Fetch a batch of labels
        :param index_batch:
        :return:
        """
        # First batch = 0 to Batch_size
        # So array [0, X] to [Batch_size-1, X]
        # this can be written as [0:Batch_size, X]
        index_first_element = index_batch * self.batch_size
        index_first_excluded_element = (index_batch + 1) * self.batch_size
        # note that from first element to first element to exclude, there are BATCH size number of items. As the second number is INDICATIVE of where to stop the indexing.
        batch_label_ndarry = self.nparray_ground_truth[
            index_first_element:index_first_excluded_element, :
        ]
        return batch_label_ndarry

    def get_batch_features(self, index_batch):
        """
        This retrieve the images of 100 images as a numpy array.
        :param index_batch:
        :return:
        """
        numpy_image_array = []  # list of PIL.Image image mode
        index_first_element = index_batch * self.batch_size
        index_final_excluded_element = (1 + index_batch) * self.batch_size

        batch_feature_ndarray = self.nparray_input_variables_15[
            index_first_element:index_final_excluded_element, :
        ]
        return batch_feature_ndarray

    def __getitem__(self, batch_index):
        batch_x = self.get_batch_features(batch_index)
        batch_y = self.get_batch_labels(batch_index)
        return batch_x, batch_y


if __name__ == "__main__":
    path_validate_spec = r"C:\GitHub\MarkerTrainer\data\dataset_validate.txt"
    validation_data = DataSequence(
        path_validate_spec, 1048576, ProcessingMode.Validation
    )
    pass
