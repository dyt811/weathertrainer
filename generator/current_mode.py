from enum import Enum


class ProcessingMode(Enum):
    Train = "train"
    Validation = "validate"
