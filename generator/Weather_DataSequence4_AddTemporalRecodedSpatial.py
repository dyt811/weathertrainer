from keras.utils import Sequence
import random
import math
import os
import numpy as np
import datetime
import logging
from generator.current_mode import ProcessingMode
from datetime import timedelta
from preparation.parse_file_datetime import parse_spec_file

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
logger = logging.getLogger()


def date_split(current_date: datetime.datetime):
    """
    Split the date into day count and hour count.
    :param current_date:
    :param format:
    :return:
    """

    new_year_day = datetime.datetime(
        year=current_date.year, month=1, day=1, hour=0, minute=0, second=0
    )

    date_differences = current_date - new_year_day
    day_of_the_year = date_differences.days + 1
    time_of_the_day = current_date.hour

    return day_of_the_year, time_of_the_day


def date_increment(
    input_date: str, number_of_three_hours_increment: int, format="%Y-%m-%d %H:%M:%S"
):
    """
    Split the date into day count and hour count.
    :param current_date:
    :param format:
    :return:
    """
    current_date = datetime.datetime.strptime(input_date, format)

    data_row_date = current_date + number_of_three_hours_increment * timedelta(hours=3)

    return data_row_date


class DataSequence(Sequence):
    """
    A customary sequence class to help obtain the input necessary for training.
    This one does factor in the temporal dimension, IGNORE spatial dimension.
    This one is customized to read the weather NPY data from the weather network challenge.
    """

    def __init__(
        self, date_txt_path: str, batch_size: int = 1048576, mode=ProcessingMode.Train
    ):

        # read the csv file with pandas
        # self.df = pd.read_csv(date_txt_path)
        path_folder = os.path.dirname(date_txt_path)

        # FILE loop > Temporal Loop > Spatial Loop

        # How many files to concatenate, there are 53! and we best not to train on one file as that is limited to 3 months period.

        """# Few files, load them all.
        if len(self.file_lines) < 5:

        # otherwise, load way more.
        else:
            num_files = 3
            for x in range(num_files):
                # Generate
                chosen_file_index = randint(0, len(self.file_lines)-1)
                chosen_file = self.file_lines[chosen_file_index]
                list_random_files.append(chosen_file)"""

        # batch size
        self.batch_size = batch_size

        # shuffle when in train mode
        self.mode = mode

        # Ground Truth Temperature from X files across N timepoints, across all possible X Y coordinates.
        self.nparray_ground_truth: np.ndarray = None
        # self.labels = self.df[['r1']].values  # THIS IS Y

        # List of variables from X files across N timepoints, across all possible X Y coordinates.
        self.nparray_input_variables_19: np.ndarray = None

        # Parse the spec file inot both files and dates
        self.files, self.datetimes = parse_spec_file(date_txt_path)

        # Loop through each file.
        for index in range(len(self.files)):

            file_name = self.files[index]
            file_start_date_time = self.datetimes[index]

            # x should have 100 x 15 x 256 x 256
            x = np.load(os.path.join(path_folder, "input_" + file_name))

            x_flattened = self.flatten_input(x, file_start_date_time)

            # Concatenate this to the object level list
            if self.nparray_input_variables_19 is None:
                self.nparray_input_variables_19 = x_flattened
            else:
                self.nparray_input_variables_19 = np.concatenate(
                    (self.nparray_input_variables_19, x_flattened)
                )

            # y should have 100 x 1 x 256 x 256
            y = np.load(os.path.join(path_folder, "label_" + file_name))

            # y_transposed should have 256 by 256 by 100 by 1
            y_transposed = y.transpose([2, 3, 0, 1])

            # y_flatten should now be 6553600, 1
            y_flatten = y_transposed.reshape(-1, y_transposed.shape[-1])

            # Concatenate this to the object level list
            if self.nparray_ground_truth is None:
                self.nparray_ground_truth = y_flatten
            else:
                self.nparray_ground_truth = np.concatenate(
                    (self.nparray_ground_truth, y_flatten)
                )

    @staticmethod
    def flatten_input(matrix_input, file_start_date_time):
        """
        This function prepares the loaded datamatrix into the right format by ruthlessly crashing the temporal and spatial dimensions. 
        :param matrix_input: 100 by 256 by 256 by 15
        :param file_start_date_time:
        :return matrix_time_inserted:6553600, 19
        """

        # matrix_transposed should have 256 by 256 by 100 by 15
        matrix_transposed = matrix_input.transpose([2, 3, 0, 1])
        logger.info(matrix_transposed.shape)
        size_x_dimension = matrix_transposed.shape[
            0
        ]  # time point is the 3rd timepoint, 2 in zero based index.
        size_y_dimension = matrix_transposed.shape[
            1
        ]  # time point is the 3rd timepoint, 2 in zero based index.
        size_temporal_dimension = matrix_transposed.shape[
            2
        ]  # time point is the 3rd timepoint, 2 in zero based index.

        # Insert placeholder 0s for X positional information.
        matrix_x_inserted = np.insert(
            matrix_transposed, 15, values=0, axis=3
        )  # x coordinate.

        # Encoding the X position information into each cell:
        for index_x in range(size_x_dimension):
            # recall the TIME in consistent across the 65536 area:
            matrix_x_inserted[
                index_x, :, :, 15
            ] = index_x  # 15 = 16th position in zero based index.
        logger.info(matrix_x_inserted.shape)

        # insert placeholder 0s for Y positional information.
        matrix_y_inserted = np.insert(
            matrix_x_inserted, 16, values=0, axis=3
        )  # x coordinate.

        # Encoding the Y position information into each cell:
        for index_y in range(size_y_dimension):
            # recall the TIME in consistent across the 65536 area:
            matrix_y_inserted[
                :, index_y, :, 16
            ] = index_y  # 16 = 17th position in zero based index.
        logger.info(matrix_y_inserted.shape)

        # insert placeholder 0s for DAY information.
        matrix_time_inserted = np.insert(
            matrix_y_inserted, 17, values=0, axis=3
        )  # day row
        matrix_time_inserted = np.insert(
            matrix_time_inserted, 18, values=0, axis=3
        )  # time row

        # Encoding the temporal data into each cell:
        for index_time_increment in range(size_temporal_dimension):
            # Timedelta to calculate the offsetted date time.
            current_index_datetime = date_increment(
                file_start_date_time, index_time_increment
            )

            # Split that into HOUR and Day of the YEAR component.
            current_yearday, current_time = date_split(current_index_datetime)

            # recall the TIME in consistent across the 65536 area:
            matrix_time_inserted[
                :, :, index_time_increment, 17
            ] = current_yearday  # 17 = 18th position in zero based index.
            matrix_time_inserted[
                :, :, index_time_increment, 18
            ] = current_time  # 18 = 19th position in zero based index.

        logger.info(matrix_time_inserted.shape)

        # Keeping the LAST dimension after the rearrangement:
        # x_flatten should now be 6553600, 19
        matrix_flattened = matrix_time_inserted.reshape(
            -1, matrix_time_inserted.shape[-1]
        )
        logger.info(matrix_flattened.shape)

        return matrix_flattened

    def __len__(self):
        # compute number of batches to yield
        return int(
            math.ceil(self.nparray_input_variables_19.shape[0] / float(self.batch_size))
        )

    def on_epoch_end(self):
        # Shuffles indexes after each epoch if in training mode
        self.indexes = range(len(self.nparray_input_variables_19))
        if self.mode == "train":
            self.indexes = random.sample(self.indexes, k=len(self.indexes))

    def get_batch_labels(self, index_batch):
        """
        Fetch a batch of labels
        :param index_batch:
        :return:
        """
        # First batch = 0 to Batch_size
        # So array [0, X] to [Batch_size-1, X]
        # this can be written as [0:Batch_size, X]
        index_first_element = index_batch * self.batch_size
        index_first_excluded_element = (index_batch + 1) * self.batch_size
        # note that from first element to first element to exclude, there are BATCH size number of items. As the second number is INDICATIVE of where to stop the indexing.
        batch_label_ndarry = self.nparray_ground_truth[
            index_first_element:index_first_excluded_element, :
        ]
        return batch_label_ndarry

    def get_batch_features(self, index_batch):
        """
        This retrieve the images of 100 images as a numpy array.
        :param index_batch:
        :return:
        """
        numpy_image_array = []  # list of PIL.Image image mode
        index_first_element = index_batch * self.batch_size
        index_final_excluded_element = (1 + index_batch) * self.batch_size

        batch_feature_ndarray = self.nparray_input_variables_19[
            index_first_element:index_final_excluded_element, :
        ]
        return batch_feature_ndarray

    def __getitem__(self, batch_index):
        batch_x = self.get_batch_features(batch_index)
        batch_y = self.get_batch_labels(batch_index)
        return batch_x, batch_y


if __name__ == "__main__":
    # Debug path
    path_validate_spec = r"C:\GitHub\MarkerTrainer\data\dataset_validate.txt"
    validation_data = DataSequence(
        path_validate_spec, 1048576, ProcessingMode.Validation
    )
    pass
