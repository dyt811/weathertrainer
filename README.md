# WeatherWiz
Using deeplearning and high-dimensional, historical, low resolution (128x128) data set to predict the weather via feature convolutional neural networks and fully connected networks before upsampling to high resolution geographical temperature predictions 

See more detailed submission documentation at Google Doc [here](https://docs.google.com/document/d/1kjPmaxSYFCnwZBuzKBO2HdkxX22KmMzplqenLAZoPrE/edit?usp=sharing). 