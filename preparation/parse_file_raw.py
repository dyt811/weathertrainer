import re
import os
import numpy as np
from datetime import datetime
from datetime import timedelta
from tqdm import tqdm, trange
import logging
from typing import List
from pathlib import Path
from PythonUtils.PUDateTime import iso2str, str2iso
from PythonUtils.folder import create

logger = logging.getLogger()


regex_train_file_name = re.compile(r"training_\d\d\d\d_\d\d\d\d.npy")
regex_validateion_file_name = re.compile(r"validation_\d\d\d\d_\d\d\d\d.npy")
regex_datetime = re.compile(r"201\d-\d\d-\d\d \d\d:\d\d:\d\d")


def parse_spec_file(path_data: str) -> (List[str], List[datetime], List[datetime]):
    """
    Specialize function to used to read date.txt, and help split out the file names and the datetimepoints
    :param path_data:
    :return:
    """
    # read the csv file with pandas
    # self.df = pd.read_csv(date_txt_path)
    path_folder = Path(path_data).parent

    file = open(Path(path_data), "r")
    file_lines = file.readlines()
    file.close()
    list_filename = []
    list_datetime_begin = []
    list_datetime_end = []

    for file in file_lines:
        success, file_name, list_datetime = parse_spec_row(file)
        if success:
            list_filename.append(file_name)
            list_datetime_begin.append(list_datetime[0])
            list_datetime_end.append(list_datetime[1])

    assert len(list_datetime_begin) == len(list_filename) == len(list_datetime_end)

    return list_filename, list_datetime_begin, list_datetime_end


def parse_spec_row(row_string: str) -> (bool, str, List[datetime]):
    """
    This process one row of such type of file specification
    :param row_string:
    :return:
    """
    if row_string == "\n":
        return False, None, None

    if not ".npy" in row_string:
        return False, None, None

    # Training file name
    filename = regex_train_file_name.search(row_string)

    # Validation file name
    filename1 = regex_validateion_file_name.search(row_string)

    # Find all text occurance of possible string.
    date_text = regex_datetime.findall(row_string)
    logger.debug(str2iso(date_text[0]))
    logger.debug(str2iso(date_text[1]))
    date_datetime = [str2iso(date_text[0]), str2iso(date_text[1])]

    if filename is None and filename1 is not None and date_text is not None:
        return True, filename1.group(), date_datetime
    elif filename is not None and filename1 is None and date_text is not None:
        return True, filename.group(), date_datetime
    else:
        return False, None, None


def process_list(path_file: str):
    """
    When we receive the data in the form of 100+.npy, this will break it into individually named npy image.
    :param path_file:
    :return:
    """
    # get the path to the folder that contain the DIR
    path_folder = Path(path_file).parent

    # Obtain the list of file to be processed and the inital time stamp of the timepoint.
    list_file, list_initial_timestamp, list_end_timestamp = parse_spec_file(
        Path(path_file)
    )

    # Make sure both lists are equally long.
    assert len(list_file) == len(list_initial_timestamp) == len(list_end_timestamp)

    path_output = path_folder / "stamped"
    create(path_output)

    for list_index in trange(0, len(list_file)):
        file_start_date_time = list_initial_timestamp[list_index]
        file_end_date_time = list_end_timestamp[list_index]

        # The input training data set to be broken down into 100 file of individual time point.
        name_input_file = os.path.join(path_folder, f"input_{list_file[list_index]}")
        process_input_files(
            name_input_file, file_start_date_time, file_end_date_time, path_output
        )

        # The label training datat sett to be broken down into 100 file of individual label stamped by their time point.
        name_label_file = os.path.join(path_folder, f"label_{list_file[list_index]}")
        process_label_files(
            name_label_file, file_start_date_time, file_end_date_time, path_output
        )
        logger.info(
            f"Finished processing Block {list_index} of timepoint (input & output) out of {len(list_file)}"
        )


def process_201807_file(path_folder: str):
    """
    A specially tailored process to take care of the file:
    # training_4800_4899.npy: 2018-06-24 00:00:00 to 2018-08-06 09:00:00
    :param path_folder: input folder which contain the data.
    :return:
    """

    name_file = "training_4800_4899.npy"

    # The input training data set to be broken down into 100 file of individual time point.
    name_input_file = os.path.join(path_folder, f"input_{name_file}")

    # The label training datat sett to be broken down into 100 file of individual label stamped by their timepoint.
    name_label_file = os.path.join(path_folder, f"label_{name_file}")

    x = np.load(name_input_file)
    y = np.load(name_label_file)

    # First batch
    file_start_date_time = str2iso("2018-06-24 00:00:00")
    file_interim_end_date_time = str2iso(
        "2018-06-30 21:00:00"
    )  # manually delineated data bypassing Jan.

    # Second batch
    file_interim_start_date_time = str2iso(
        "2018-08-01 00:00:00"
    )  # manually delineated data bypassing Jan.
    file_end_date_time = str2iso("2018-08-06 09:00:00")

    # Calculate the number of three hour interval needed between these two time points.
    n_intervals = (file_interim_end_date_time - file_start_date_time) / timedelta(
        hours=3
    )
    intervals = int(n_intervals) + 1
    x1 = x[0:intervals, :, :, :]
    x2 = x[intervals : x.shape[0], :, :, :]

    y1 = y[0:intervals, :, :, :]
    y2 = y[intervals : x.shape[0], :, :, :]

    process_input_data(
        x1,
        file_start_date_time,
        file_interim_end_date_time,
        os.path.join(path_folder, "stamped"),
    )
    process_input_data(
        x2,
        file_interim_start_date_time,
        file_end_date_time,
        os.path.join(path_folder, "stamped"),
    )

    process_label_data(
        y1,
        file_start_date_time,
        file_interim_end_date_time,
        os.path.join(path_folder, "stamped"),
    )
    process_label_data(
        y2,
        file_interim_start_date_time,
        file_end_date_time,
        os.path.join(path_folder, "stamped"),
    )

    logger.info(f"Finished. training_4800_4899")


def process_201801_file(path_folder: str):
    """
    A specially tailored process to take care of the file:
    # "training_3600_3699.npy: 2017-12-25 00:00:00 to 2018-02-06 09:00:00"
    :param path_folder: input folder which contain the data.
    :return:
    """

    name_file = "training_3600_3699.npy"

    # The input training data set to be broken down into 100 file of individual time point.
    name_input_file = os.path.join(path_folder, f"input_{name_file}")

    # The label training datat sett to be broken down into 100 file of individual label stamped by their timepoint.
    name_label_file = os.path.join(path_folder, f"label_{name_file}")

    x = np.load(name_input_file)
    y = np.load(name_label_file)

    # First batch
    file_start_date_time = str2iso("2017-12-25 00:00:00")
    file_interim_end_date_time = str2iso(
        "2017-12-31 21:00:00"
    )  # manually delineated data bypassing Jan.

    # Second batch
    file_interim_start_date_time = str2iso(
        "2018-02-01 00:00:00"
    )  # manually delineated data bypassing Jan.
    file_end_date_time = str2iso("2018-02-06 09:00:00")

    # Calculate the number of three hour interval needed between these two time points.
    n_intervals = (file_interim_end_date_time - file_start_date_time) / timedelta(
        hours=3
    )
    intervals = int(n_intervals) + 1
    x1 = x[0:intervals, :, :, :]
    x2 = x[intervals : x.shape[0], :, :, :]

    y1 = y[0:intervals, :, :, :]
    y2 = y[intervals : x.shape[0], :, :, :]

    process_input_data(
        x1,
        file_start_date_time,
        file_interim_end_date_time,
        os.path.join(path_folder, "stamped"),
    )
    process_input_data(
        x2,
        file_interim_start_date_time,
        file_end_date_time,
        os.path.join(path_folder, "stamped"),
    )

    process_label_data(
        y1,
        file_start_date_time,
        file_interim_end_date_time,
        os.path.join(path_folder, "stamped"),
    )
    process_label_data(
        y2,
        file_interim_start_date_time,
        file_end_date_time,
        os.path.join(path_folder, "stamped"),
    )

    logger.info(f"Finished. training_3600_3699")


def date_increment(input_date: datetime, number_of_three_hours_increment: int):
    """
    Split the date into day count and hour count.
    :param current_date:
    :param format:
    :return:
    """
    data_row_date = input_date + number_of_three_hours_increment * timedelta(hours=3)

    return data_row_date


def date_increment_str(
    input_date: str, number_of_three_hours_increment: int, format="%Y-%m-%d %H:%M:%S"
):
    """
    Split the date into day count and hour count.
    :param current_date:
    :param format:
    :return:
    """
    current_date = datetime.strptime(input_date, format)

    data_row_date = current_date + number_of_three_hours_increment * timedelta(hours=3)

    return data_row_date


def process_label_data(
    y: np.ndarray, timestamp_start: datetime, timestamp_end: datetime, path_out
):
    """
    Core function to process output label data.
    :param y:
    :param timestamp_start:
    :param timestamp_end:
    :param path_out:
    :return:
    """

    # How many 3 hours interval expected:
    timestamp_end_calculated = date_increment(timestamp_start, y.shape[0] - 1)
    if not timestamp_end == timestamp_end_calculated:
        logger.warning(
            f"Endpoint mismatching! Quitting processing this particular label file corresponding to {timestamp_start.isoformat()}"
        )
        return

    # Loop through each of the 100 observation.
    for index_observation in trange(0, y.shape[0]):

        # Extract the three hourly measures
        y_3hourly = y[index_observation, :, :]

        # Increment the timestamp.
        timestamp_incremented = date_increment(timestamp_start, index_observation)

        # Convert the timestamp to compatible file name on Win
        name_file = "label_" + iso2str(timestamp_incremented)

        # Form absolute path.
        path_file = os.path.join(path_out, name_file)

        # Save to the updated path.
        np.save(path_file, y_3hourly)

        # Visualization check.
        # visualize(y_3hourly[0, :, :], f"Variable: Ground Truth Temperature")

    return


def process_label_files(
    path_file: str, timestamp_start: datetime, timestamp_end: datetime, path_out=""
):
    """
    Takes a file which contain 100 validation output and break it into 100 smaller files.
    :param name_file: The name of the file containing 100 observations to be processed and saved as separate files.
    :param timestamp_start: the timestamp of the initial timepoint of the 100 observations.
    :return:
    """
    from evaluation.visualize import visualize

    # Default the outpath to use the path of the file where it come from
    if path_out == "":
        path_folder = os.path.dirname(path_file)
    else:
        path_folder = path_out

    # x should have 100 x 15 x 256 x 256
    y = np.load(path_file)

    # Call the underlying low level function.
    process_label_data(y, timestamp_start, timestamp_end, path_folder)
    return


def process_input_data(
    x: np.ndarray, timestamp_start: datetime, timestamp_end: datetime, path_out
):
    """
    Takes a file which contain 100 observations and break it into 100 smaller files.
    :param name_file: The name of the file containing 100 observations to be processed and saved as separate files.
    :param timestamp_start: the timestamp of the initial timepoint of the 100 observations.
    :param timestamp_end:
    :param path_out:
    :return:

    from evaluation.visualize import visualize

    # Default the outpath to use the path of the file where it come from
    if path_out == "":
        path_folder = os.path.dirname(path_file)
    else:
        path_folder = path_out
    """

    # How many 3 hours interval expected (since starting timestamp is already given. Then only however many more to go: hence, the typical dimension - 1
    timestamp_end_calculated = date_increment(timestamp_start, x.shape[0] - 1)
    if not timestamp_end == timestamp_end_calculated:
        logger.warning(
            f"Endpoint mismatching! Quitting processing the particular input file corresponding to: {timestamp_start.isoformat()}"
        )
        return

    # Loop through each of the 100 observation.
    for index_observation in tqdm(range(0, x.shape[0]), position=1):

        # Extract the three hourly measures
        x_3hourly = x[index_observation, :, :, :]

        # Increment the timestamp.
        timestamp_incremented = date_increment(timestamp_start, index_observation)

        # Convert the timestamp to compatible file name on Win
        name_file = "input_" + iso2str(timestamp_incremented)

        # Form absolute path.
        path_file = os.path.join(path_out, name_file)

        # Save to the updated path.
        np.save(path_file, x_3hourly)

        # reduce the data dimension by identifying and isolating the layer with redundant input
        # Non time varying dimensions:
        """    
        x_3hourly_MGD = x[index_observation, 0, :, :]
        x_3hourly_MED = x[index_observation, 1, :, :]
        x_3hourly_ZPD = x[index_observation, 2, :, :]
        x_3hourly_VGD = x[index_observation, 3, :, :]
        """

        # Visualization check.
        """         
        for index_variable in range(0, x_3hourly.shape[0]):
            # Example visualization of the 15 layers.
            visualize(x[index_observation, index_variable, :, :], f"Variable:{index_variable+1}")
        """
    return


def process_input_files(
    path_file: str, timestamp_start: datetime, timestamp_end: datetime, path_out=""
):
    """
    A wrapper which takes a file which contain 100 observations and break it into 100 smaller files.
    :param name_file: The name of the file containing 100 observations to be processed and saved as separate files.
    :param timestamp_start: the timestamp of the initial timepoint of the 100 observations.
    :param timestamp_end:
    :param path_out:
    :return:
    """
    from evaluation.visualize import visualize

    # Default the outpath to use the path of the file where it come from
    if path_out == "":
        path_folder = os.path.dirname(path_file)
    else:
        path_folder = path_out

    # x should have 100 x 15 x 256 x 256
    x = np.load(path_file)
    process_input_data(x, timestamp_start, timestamp_end, path_folder)
    return


if __name__ == "__main__":
    # process_list(r"C:\GitHub\MarkerTrainer\data\date.txt")
    process_list(r"E:\WeatherData\data\date.txt")

    process_201801_file(r"E:\WeatherData\data")
    process_201807_file(r"E:\WeatherData\data")

    pass
