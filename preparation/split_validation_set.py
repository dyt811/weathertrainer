import os
import shutil
import numpy as np
import logging

logger = logging.getLogger()

# Sets of functions to get everything organized so they can be fed into the Generator class and get data rolling.


def write_dataset_validate(data_folder):
    os.chdir(data_folder)
    file = open("dataset_validate.txt", "w+")
    file.write("input_validation_0000_0123.npy: 2018-01-16 12:00:00" + "\n")
    file.write("input_validation_0123_0247.npy: 2018-07-16 12:00:00" + "\n")
    file.close()


def write_dataset_train(data_folder):
    os.chdir(data_folder)
    shutil.copy("date.txt", "dataset_train.txt")


def write_selected_training_set(data_folder):
    """
    Write only summer list related to the Jan July months.
    :param data_folder:
    :return:
    """
    from itertools import chain
    from operator import itemgetter

    os.chdir(data_folder)
    file = open("dataset_train.txt", "r")
    file_lines = file.readlines()
    file.close()

    file = open("dataset_train.txt", "w+")

    # index row of the data more closer to jan and jun to reduce the amount of the data necessary to train.
    selected_lines = chain(range(6, 11), range(18, 25), range(33, 38), range(45, 50))

    for line in selected_lines:
        file.write(file_lines[line])
    file.close()


def split_train_evenly(data_folder):
    """
    Split training data evenly into train and validate dataset.
    :param data_folder:
    :return:
    """
    os.chdir(data_folder)

    # Read all train data.
    train_file = open("dataset_train.txt", "r")
    train_file_lines = train_file.readlines()
    train_file.close()

    # Read all validate data.
    validate_file = open("dataset_validate.txt", "r")
    validate_file_lines = validate_file.readlines()
    validate_file.close()

    # Get ready to Write both files.
    train_file = open("dataset_train.txt", "w+")
    validate_file = open("dataset_validate.txt", "w+")
    validate_file.writelines(validate_file_lines)

    for index_line in range(len(train_file_lines)):
        # Write all odd rows.
        if index_line % 2 == 1:
            train_file.write(train_file_lines[index_line])
        else:
            validate_file.write(train_file_lines[index_line])

    train_file.close()
    validate_file.close()


def split_validation(data_folder):
    """
    Validation dataset is NOT given in the same format as the other training dataset. A reformatting is NECESSARY for the data generator to ensure they can be loaded into the array. Sigh.....
    Useful later on when we need to extract respective temporal information
    :param data_folder:
    :return:
    """
    os.chdir(data_folder)

    logger.info("")

    validation_input = np.load("input_test_set.npy")

    test_partA = validation_input[0:124, :, :, :]
    np.save("input_validation_0000_0123.npy", test_partA)

    test_partB = validation_input[124:248, :, :, :]
    np.save("input_validation_0123_0247.npy", test_partB)

    label_validation = np.load("label_test_set.npy")

    test_partA = label_validation[0:124, :, :, :]
    np.save("label_validation_0000_0123.npy", test_partA)

    test_partB = label_validation[124:248, :, :, :]
    np.save("label_validation_0123_0247.npy", test_partB)


if __name__ == "__main__":
    data_folder = r"C:\Temp\WeatherChallenge\data"
    split_validation(data_folder)
    write_dataset_train(data_folder)
    write_dataset_validate(data_folder)

    split_train_evenly(data_folder)
    # write_selected_training_set(data_folder)
