2019-01-14T200623EST

#Transpose the tensors to have format (NHWC) / numpy array format given is (NCHW)
Num_samples x Height x Width x Channels
vs
N, Channel, Height, Width.

This is an issue related to Theano format vs Tensorflow... sigh.

So each INPUT.npy file has
100 x 15 x 256 by 256. meaning the data.

100 timepoint x 15 variables:
- MGD: Difference between rdps and caldas for the variable MG (Water/land mask)
- MED: Difference between rdps and caldas for the variable ME (Mean Elevation of Topography)
- ZPD: Difference between rdps and caldas for the variable ZP (Roughness length (CRESSMAN))
- VGD: Difference between rdps and caldas for the variable VG (Dominant vegetation type)
- TD: Dew point temperature
- TT: Air temperature
- PN: Sea level pressure
- NT: Total cloud cover
- H: Height of boundary layer
- RT: Total precipitation rate
- I4: Water in the snow pack
- 5P: Fraction of grid covered by snow
- I6: Albedo of snow
- UU: U-component of the wind (along the X-axis of the grid)
- VV: V-component of the wind (along the Y-axis of the grid)
per x coordinate. and y xoodinate.

So each LABEL.npy file has
100 x 1 x 256 by 256.
1 single variable is temperature.

2019-01-14T202843EST
In essnece, the 256 of input are derived from lower resolution data set. So Their real world resolution is actually upsampled probably from 64 x 64 TO 256 by 256.

The CNN has to be able to take the original data. Augment it by at least the four directions (since graphic based augmentation won't just work here..., all 15 dimension of variable data needs to be augmented.

Input dimension is 15 by 256 by 256. output is 1 by 256 by 256.

2019-01-14T203342EST

so to recap, we have high resolution 1) 14 variables. 1 low resolution spatial data, but everything is binned to high resolution space. This probably simplify my life a lot already. Since the Input output are mostly converted properly, we are looking at no upsampling problem as previously described so really, it is a lot more straight forward.

2019-01-14T205641EST
It is a dimension reduction type approach which these supersampling deep neurnal network is not really utilizing because the other source data from the proportly 14 sources are all high resolution. We have tons of data to come up with the approach as to WHY the final temperature is what it is this clealy needs tons of data augmentation).

2019-01-21T180743EST
So the data has both spatial and temporal component. Success predictor likely has to take that into account.
Input: 100 (time) x 15 (measurements) x 256 x 256 (space)
Input: 100 (time) x 1 (temperature) x 256 x 256 (space)

The core here... the measurement dimension is too high. Ideally, we should compress that informaiton to ONE dimension, then after considering the spatial and temporal component, it should then approximate the final temperature output.

To recap: Stage 1: compress 15 to 1 > This is VERY unlikely to be close to the final temperature as it has NO spatial or temporal information. However, if it is good enough, I will submit this as we then totally do not need the spatial/temporal component.
Stage 2: After this composite index is produced. We train this versus the final output.

We will commence with stage 1 of the analyses: trying to ignore spatial and temporal component and produce a network that takes 15 measurements and produce one outcome. This can be done via classic ML before we introduce the CNN component to it.

Ideally, we could use a N dimension CNN to solve directly to end results but that 1) may overfit, 2) probably take forever to train 3) takes massive amount of GPU ram which I don't have. Simpliest solution is probably better, hence ML before CNN.

I initially thought this was a upsampling problem which there are some interesting article about but given that the other 15 variables are in high resolution, it is actually not a simple upsampling issue (also, that 64 x 64 temperature  data was not directly given)

2019-01-21T183220EST
Looking back, since temperature is very temporal dependent, I need to include that in the linear model. Space... however, given that many variable cover such information, it should work without that for now. Later on, CNN can make help with that?

2019-01-21T184031EST
Python environmental management is like.... a dark art. Anaconda 3.7 will ruin lives. Had to manually manage via Conda to maintain 3.6 to keep sane... because TensorFlow only support 3.6 as of today...

2019-01-21T190427EST
So if I encode date time into days of the year etc, and encode the spatial grid inforamtion... that technically has all the information for the classifier to try to chew on BEFORE ever touching CNN as this way, EVERY pixel of the spatial data becomes a sample and this would vastly increase the number of observation available.

2019-01-21T192938EST
I managed to serialize the time component into its own nth hour of the year.
This allow convert the array into: 16 variable (15 + time) > 1 outcome. for 65536 observation. for one geographical regions.

2019-01-21T195316EST

Gonna build a series of models:
# Input: 15 variable
# Output: temperature
# N = x * y * timepoint
# without regard to datetime, location.

# Input: 15 variable + time of the year
# Output: temperature
# N = x * y * timepoint
# without regard location.

# Input: 15 variable + time of the year + x coordinate + y coordinate.
# Output: temperature
# N = x * y * timepoint

2019-01-21T205214EST
So the function was written to conver to 15 variable output, essentially stripping time and location information to massively boost N. This model should not be super accurate but hey, at least using the same architecture, by checking if we can predict with minimal information, we avoid vastly overfit. I still have to write the function that actually batch the data up and send it to Keras to run the analyses but the network architecture chosen so far is fully connected dense layer with random dropout and batch normalization to avoid overfitting. Dense network for 15 var to 1 output ANN is probably way over kill so I kept the architecture simple: 8x4x2x1 and regress to a dense = 1. 2+ layers help with many things already. I expect this network to perform well enough that at least it should not be a couple hundred degrees off the prediction. Now I just need to find ways to load the millions upon millions of records into the neural network easily and effectively. :D

2019-01-24T183029EST
So today, I mainly need to focus on building the necessary generator to load from the NPY file batches. The simple network architecture will suffice for now to do a simple analyses with respect to perhaps one input files. A true GPU based training will be necessary to train to be able to generalize beyond the given niche month per file.

2019-01-24T185232EST
Probably going to soon need a function to convert date into a day of year to be applied to the data as loading happens. I staarted something but this time I need to couple everythign witht he WeatherDataSequence based on my previous TF training dataset providence.


2019-01-24T202758EST
Mostly done refactoring the class to actually load NPY data as a data sequence. Still have some issues juggling with numpy array versus pandas dataframes but it might actually end up easier. I think this further iterative extension of existing project and repurpose for output is actually quite efficient and make it possible for me.

2019-01-28T180044EST
Today's objective to finish the loader to allow the training to commence with SIMPLE non-spatial and non-temporal data before bridging it with the simple customized network.

2019-01-28T182725EST
Struggling quite a bit with Numpy array as I am much more used to work with dataframe higher level objects. Yet to reduce overhead it is best to stick within numpy as that is what goes into TensorFlow objects anyway in the end.

2019-01-28T184407EST
After some struggle and more annotation of the code, the generator sequence for weather related data is working. Although I am not sure if there are sufficient memory to load all data and how it would increase the time of training by storing things in CPU memory before shuffle over to GPU memory for training but I think we have a basic workflow down. We can start training without major impedence at this point. Except the customized cost function definnition.

2019-01-28T185844EST
The validation data set cannot really be used easily without adapting the exising function. Gonna have to do that in addition to the cost function before we can move with the training :( The very fact that the verification dateset is not in the proper temporal order means I have to either split it into two parts or manually code something to deal with the temporal stripping functions later on.

2019-01-28T202632EST
Finally able to parse the data files as two separate files. Working great so far.

2019-01-28T205829EST
Now that most of the data generator related stuff are sorted out, I think we are in good shape. This only apply for temporal and spatial strippped data. They will have to be extended in the future to include those data types and write out into datafiles even or dynamicly add them in as needed. But FIRST we must train a network that don't need CNN first to see how much prediction can be done WITHOUT sptial temporal component.

2019-02-06T180217EST
Been a while. Getting back into the analyses. I gave it a bit more thought and realized that unless i push really hard over the next few days, I most likely not going to be able to meet the submission deadline. I will definitely have to at least generate the first output training model over the next little.

2019-02-06T184533EST
Fixed enough patch work to the problematic section to allow the full pipeline to somewhat work. The Keras model is built, the keras are being run hopefully, right now with a very small amount of data but soon larger. Data generator should fully help over come this issue. Found a bug with __len__ implementation of the weather data sequence. Gonna have to fix it before training. So close. Yet so far.

2019-02-06T185232EST
What I typically find is that whenever I manage to get a training going, it is super exciting!
And SO FAR! WE HAVE TRAINING! Sure the accuracy is shit and loss is massive but at least, the pipeline basic concept is going. This is very encouraging. Now, I just need to find addition ways to encode both spatial as well as temporal data to be complementary to the 15 variable input and then see how it goes.

2019-02-06T191721EST
As to no surprise, the loss barely budged and MAE is super stebly high as well. Suggesting that we are probably not having enough data to work with to generalize pattern, which makes sense as I literally just randomly pulled some dates time and try to force them down a network. I gotta start build a network that takes into account of the time of the year encoded.

2019-02-06T201951EST
Collapsing temporal dimension turned out to be a bit more challenging than I realized but also great time to learn more Numpy related matrix manipulation at low level. One insertion command later and one replace command and I am there. Boom shakakaka mehehahahaha

2019-02-06T210549EST
Great!  I managed to encode both spatial and temporal into the variables which are fed into the neural network. I can see MAE definitely went down, but I am not sure if it actually helps and improved over the long wrong. The loss for SURE went down but maybe I am over analyzing it. For, now it is just a matter of optimization and accuracy and update of the architecture and let it train multiple times over night I suppose. :D

2019-02-07T190619EST
Found a bug that is preventing logs and model from being checkpoint. Fixed typo and now tensorboard is now working.


2019-02-08T232500EST
Been playing with the network architecture a bit and now have a basic high dimensional regressional network going. The training accuracy is about 0 with loss plateau around 100 epochs. Something is not right as the val_accuracy is just about 100% 0 flat. I suspect I am not loading them correctly for the validation set data. Also, I think some feature engineering with regard to cyclicity of the season and hour of the day needs to be done to help the model out a bit. Combining both is fine and with a lot of data, model can probably figure it out but if I want to build a efficient model in short amount of time optiming for the June Jan prediction, I should focus primarily on those two months and see. So far, the models are not doing anything. Completely useless. But this is where the fun begins as experimentatation with architecture and

2019-02-09T1427EST
Separating the time into discrete component seems to have helped. I also realized that perhaps "accuracy" in the context of regression is meaningless. MAE and other differences is a much more valuable and USEFUL measure. I think for now, I am getting about 5 degrees differences from the ground truth. Not good but by all means, not as bad as the 10+ degrees MAE I was getting before. watching tensorboard update is quite... addictive...Seems to be getting closer I suppose?

2019-02-09T1529EST
Took me a while... before I realize the model checkpoint function is recording model to maximize mean absolute error hence never saving... LOL.

2019-02-09T1541EST
You know I am sadistic when you see my training loss is higher than validation loss. I am a TRUE believer of self tortue and OCD dropout data. hahahaha.

2019-02-09T1649EST
Finished building the visualizer. Still have to build the scorer though.

2019-02-09T1745EST
Got the scorer done. Looking FABULOUS indeed even though the performance is probably a bit garbage. i.e. I think the result is worse than the AIR temperature low resolution alone. Visually, it looks actually more resemble dew point temperature measurment though but DEFINITELY look better than noise so the training DID work somehow.

 2019-02-09T1752EST
 Too bad the competition had such a tight deadline that even though I spend almost close to a week full time equivalent on this, the results are still not optimal. however, I think I might be hitting close to the limit of my approach. Without considering neighboring temperature, potential information are missed. When you don't have enough information, accuracy suffer now matter the training time.

 Going to try one or two more training with much lower dropout and send this baby to town.

 2019-02-11T1801EST
 So did manage to build the entire facility necessary to run the the temporal flattened data input as an array. However, they are actually not as good as I had hoped and simpler model seems to have done better. Yesterday I attempted to actually split trianing data into halves and validate along with the two additional, actually produced better results than I expected with mean absolute difference of around 3.98! where as prior attempt barely reached 5.

 Great to find out the deadline has been pushed back a week to give me more time to finetune the model. Wise move. Now I can do more documentation and wrap up of everything else to ensure whatever improvement made will contribute back to the raw source code of marker trainer.

 So currently, I am using first half to train, second half to validate (plus the two validation set). This will absolutely avoid overfitting as the data is COMPLETELY unseen. And the current best performing model is only 60kb! Absolutely insanely tiny when using 19 input 1 output with literally millions of observations!


2019-02-11T1855EST
So far, the model suggest the way I am implementing the Conv2d at least is not very good becasue it seems to be way overfitting at layer level and validation is terrible. despite impressive mean absolute error on training dataset of 3! Well, the best way to tell is when I finish building the BATCH score calculation to see how these model will REALLY do across the entire dataset

2019-02-11T2013EST
Visual inspection of the convolution model suggest MANY noisy input were produced. This is most likely a DIRECT result of dropout.


2019-02-12T193419EST
In doing some quick search suggest, some form of recurrent neural network architecture might be extremely beneficial.
https://towardsdatascience.com/temperature-prediction-using-recurrent-neural-network-fd6b8436b9c9

Too bad I most likely won't have time to fully revamp the architecture to adopt a recurrent network so I will have to leave those as homework.

This article mentioned that the RSME is around 2 degree here: http://sci-hub.tw/https://www.sciencedirect.com/science/article/pii/S0378778815300839

So I definitely still have s ome serious training to do


2019-04-02T201934EST
Resuming analyses.

After almost two months of hiatus, I decided to resume the work on the weather prediction challenge. A lot of interesting ideas have been sprung from the competitiion but I never managed to actually see them through.

Also, numerous important mistakes were spotted near the end of the competition that SEVERELY limited my competitiveness of the submission. Therefore, it is imperative that I fix all these issues and bring this competition submission to its rightfully deserving end. I documented the list of grevious errors I made last time here: http://blog.windstalker.net/blog-posts/

Namely, the ACTUAL feasibility of the two modelling approach (19Vin1Vout based at per unit grid of the spatial 256 x 256 grid) and the whole grid output approach (CNN approach). I have just secured a Titan X to help with the expedited training process.

2019-04-15T184401EST
Finally hosting a meetup where no one comes. This would give me some quality time to actually work on the code. Today's goal is to convert the data to per 3H timepoint format so allow better facilitation.

2019-04-15T195541EST
Finally took the time to build a general purpose function to VISUALIZE what the underlying data structure looks like. Quite cool stuff. I might even make movie out of this. It is interesting that the underlying data is probably diagonally aligned hence making it even more challenging. The first Four variables are actually far more challenging being higher dimensional but NON time varying potentially.

2019-04-23T183104EST
Continuing to work on this man.

I was looking through
training_3600_3699.npy: 2017-12-25 00:00:00 to 2018-02-06 09:00:00 and
training_4800_4899.npy: 2018-06-24 00:00:00 to 2018-08-06 09:00:00 and

I think the timepoint rules needs to be very specially handled here.
2018 Jan is completely skipped over.
SO is the 2018 July.

I just realized these two training sets probably have data that do not belong there and needs to be specially treated as the data/hours prediction is quite a bit off.
Now updating code to actually check both the beginning as well as the end of the data set. This was also another point of great problem for the older analyses.

2019-04-23T200856EST
This time date thing is REALLY getting on my nerver with regard to ISO conversion back and forth so I will move forward and put them into my library.

2019-04-30T181024EST
Today's goal is to really ensure that the individual file datetime conversion is working 100% reliably and saved properly at the respective locations.
Solved an old legacy issue. Apparently strptime does not take keyword based argument. Interesting.

2019-04-30T184130EST
Sigh. Forgot a layer of loop that parses the INDIVIDUAL ~100 timepoints trial block. This needs to be parcellated out into its own chunks. I forgot this layer of complexity entirely. Damn.

2019-04-30T184524EST
Forgot the fact that I solved that problem last time already. Such is the perile of programming post dinner while distracted.

2019-04-30T190510EST
Finally, I can saw that most data are being laid to rest properly in its own miniture IO format to allow more truer per image level randomization.

The two special files are 3600_3699 and 4800 4899. Those needs special routine to handle them it seems.

Which are:
training_3600_3699.npy: 2017-12-25 00:00:00 to 2018-02-06 09:00:00
training_4800_4899.npy: 2018-06-24 00:00:00 to 2018-08-06 09:00:00


2019-04-30T194630EST
Doing some refactoring for the parse_file_raw.py class to make it more generic and can be used in a few more cases than originally intended.


2019-04-30T205438EST
Finally handled those two cases to generate HIGHLY specific data output so I can properly hot load them as needed be into memory for training.


2019-04-30T210037EST
Something is still wrong with the JUne end / August beginning data. Some large chunks of it missing for NO REASON!


2019-05-07T190957EST
Successfully finished today's goal of making a MOVIE rendering per the variable across the timepoint. The renderings may not be the fanciest but it works and looks pretty and that is COOl. It does show some static image data set as the competition guideline mentioned.

A quick rendering of 100 frames was rather short 3 secs video shows a lot interesting insight already.


2019-05-21T192757EST
Holy smoke. Two weeks later, and that rendering is FINALLY almost wrapping up. I managed to make them all into a play list and MULTIPLEX them. Pretty cool. but super demanding on the hardware. Most videos are uploaded.
http://viewsync.net/watch?v=yjjOIL3HGbM&t=0&v=iIfPLapfhPo&t=0&v=iHlrskQDyF8&t=0&v=2OaKHg4mcrc&t=0&v=WDTVstBbUh4&t=0&v=c9gKmGz8_rQ&t=0&v=QZ7g81kdbN4&t=0&v=lH4D5Hfk-nE&t=0&v=Kirl8kN1o0k&t=0&v=0QGTb3rqEqg&t=0&v=9i0nBM6943I&t=0&v=smFf7Ih7Qmg&t=0&mode=solo
Based on this, I can conclusively say that theire is a daily variation pattern and a very strong one.

2019-05-21T202917EST
Okay, just realized that I can probably speed up the training by MANY fold by simply discarding the first four variables as they are FOREVER the same in the time period and is part of the geospecific concerns.

2019-05-28T185526EST
Great! Now that I spent the time to visualized the data and build a semi functioning prototype data loader, it is time to dive into training parts. It took quite a while but finally, getting my hands dirty.
The overall data will demand massive amount of memory.

2019-05-28T190530EST
I realized that the mistake with generator class I built was not really memory efficient. It actually doesn't use the index loading necessary to prevent data overflow. This parts needs to be completely rewritten in order to the actually truly randomize the data.

2019-05-30T001906EST
Okay, initial training reshaping is working out okay. Some issues remains with regard to the list index out of range problem that I am seeing.

In addition, I think without incorporating temporal information at the dense layer level, it would be extremely difficult.

That would be the perfect time to introduce the additional information from the previous layers as well as the date timestamples to substantially reduce the spatial complexity of the fitting process. Currently preliminary training trials show inability to learn.

2019-06-22T100258EST
Deadline is in 7 days. Took me a while to recover from the vacation mode. Still needs to implement the constant and variable input component to the network.
Current priority is to resolve a bug that led to batch_index out of bound.
This is when trying to draw an index from the draw_indices

There is a total of 5342 NPY file for these timepoint.

First element: INDEX x BATCH SIZE
Final excluded element: INDEX+1 X BATCH SIZE

This always seems to happen at the SECOND iteration.

So as feared, somehow, the index exceeded higher bound, the issue is HOW did this come to be.
For some reason, the index_final_excluded_element was calculated to be 5344.

The index batch was 333 + 1 x 16 resulted in  5344

The problem I am running into is why is index batch out of bound...
The len function is probably not describing this properly but making the generator think that there are more data than it looked.

2019-06-22T110622EST
The reason I am usually getting second batch issue is LIKELY due to the randomization seed. Which ensured reproducibility. Great luck I incorporated that.  Deterministic reproducibility really helps with debugging process.

if this bug is resolved, the next stage is about adding the constant 4D terms into the dense layer and adding the time date information.

Okay: here: https://github.com/keras-team/keras/issues/1330

As predicted, I didn't need to tap all the way into TensorFlow layer to do this and keras is sufficient. GOD DAMNNED IT THIS IS SO FUCKING COOL. I have been dreaming of doing this type of cool network architecturing for AGES. Finally getting it done is so freaking awesome.

2019-06-22T112726EST
Okay, the previous bug was fully resolved. Time to tackle the elephant in the room. Update the model with constant input and variable inputs.
THe constants are the geographical features that are spatially dependent.

The more easier to integrate are probably the variable time date input.

So there are two continuous variables that needs to be incorporated to the dense layer:

time of the day:
day of the year:

2019-06-22T235522EST
The training is still ongoing but unlikely to vastly improve.

https://stackoverflow.com/questions/42556919/adding-a-variable-into-keras-tensorflow-cnn-dense-layer has a very good insight into how to incorporate additional variables.

Most likely, I can incorporate the constant from those fixed stack layers at the model architecture stage during layer specification.

2019-06-24T121259EST
Apparently, this will tap into the KERAS functional API, which is a great time for me to learn about it.

2019-06-24T140240EST
So apparently, we can specify multiple input into KERAS modeling. I feel better, sleep better and is going to rest bette rnow.

2019-06-24T222810EST
Including the day of the year and time of the day information certainly improve the modeling performaqnce by reducing mean absolute error to 4.9 after one single day of training. Where as before, the training reached 4.0 best performance ever after 500 epoch over 4 days of computing.

2019-06-26T082613EST
Oddly, adding time separately did not actually improve the training performance. However, the network architecture was also tweaked so that cannot be ruled out.

Added another convolution stack and doing one final training to see.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Interesting things to explore in the near future
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1) Prediction of a timepoint as in which season it belongs.
2)



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Several important things to fix:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Fixed:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1) Convert data from NPY per 3h timepoint, save the 5342 files， per time point.
    This dataset 1 is for PER CNN map type of training.
2) Convert data from NPY per 3h timepoint, save the 5342 x 256 x 256files， per time point.
3) Fully randomize the dataset.