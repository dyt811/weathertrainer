import numpy as np
from keras.models import Sequential
from generator.Weather_DataSequence4_AddTemporalRecodedSpatial import (
    date_increment,
    date_split,
)
import os
import logging
from datetime import datetime
from PythonUtils.PUDateTime import datetime_increment, str2iso

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
logger = logging.getLogger()


data_dir = r"E:\WeatherData\data"


def run_CNN_model_on_data(
    input_CNN_model: Sequential,
    filename: str,
    file_starting_date: str = "2018-01-16 12:00:00",
):
    """
    Run a ANN prediction base on the model on the path. This does not deal with CNN model.
    :param input_CNN_model: the input TF flow.
    :param file_path:
    :return:
    """
    list_timepoint = []
    list_prediction = []
    list_dew_temp = []
    list_air_temp = []
    list_truth = []

    # load 4D data for ALL timepoint:
    data_input = f"input_{filename}"
    matrix4d_raw_data = np.load(os.path.join(data_dir, data_input))

    data_label = f"label_{filename}"
    matrix4d_label = np.load(os.path.join(data_dir, data_label))

    # Loop through all timepoint.
    size_temporal_dimension = matrix4d_raw_data.shape[
        0
    ]  # time point is the 1st position.
    for index_temporal in range(size_temporal_dimension):

        matrix3d_timepoint_specific = matrix4d_raw_data[
            index_temporal, :, :, :
        ]  # should be 15, 256, 256
        matrix3d_timepoint_specific_padded = np.expand_dims(
            matrix3d_timepoint_specific, 0
        )
        specific_timepoint = date_increment(file_starting_date, index_temporal)

        matrix2d_predicted_temp_padded = predict_via_CNN(
            specific_timepoint, matrix3d_timepoint_specific_padded, input_CNN_model
        )
        matrix2d_predicted_temp = np.squeeze(matrix2d_predicted_temp_padded)

        # 6th variable is the index of the low resolution temperature model
        matrix2d_low_res_air_temp = matrix4d_raw_data[
            index_temporal, 5, :, :
        ]  # 256x256x1

        # 5th variable is the index of the dew temperature
        matrix2d_low_res_dew_temp = matrix4d_raw_data[
            index_temporal, 4, :, :
        ]  # 256x256x1

        ground_truth_temp = matrix4d_label[index_temporal, 0, :, :]

        # Adding them to several lists.
        list_prediction.append(matrix2d_predicted_temp)
        list_dew_temp.append(matrix2d_low_res_dew_temp)
        list_air_temp.append(matrix2d_low_res_air_temp)
        list_truth.append(ground_truth_temp)
        list_timepoint.append(specific_timepoint)

    return list_truth, list_prediction, list_air_temp, list_dew_temp, list_timepoint


def run_CNN_temporal_model_on_data(
    input_CNN_model: Sequential, filename: str, file_starting_date: str
):
    """
    Run a CNN_temporal prediction base on the model on the path. These are for CNN models where time information is not incorporated.
    The 3D matrix needs to be actually about 11x256x256 to be fed into the machine learning model
    2019-06-26T231637EST
    :param input_CNN_model: the input TF flow.
    :param file_path:
    :return:
    """
    list_timepoint = []
    list_prediction = []
    list_dew_temp = []
    list_air_temp = []
    list_truth = []

    # load 4D data for ALL timepoint:
    data_input = f"input_{filename}"
    matrix4d_raw_data = np.load(os.path.join(data_dir, data_input))

    data_label = f"label_{filename}"
    matrix4d_label = np.load(os.path.join(data_dir, data_label))

    # Loop through all timepoint.
    size_temporal_dimension = matrix4d_raw_data.shape[
        0  # time point is the 1st position.
    ]

    for index_temporal in range(size_temporal_dimension):

        """
        matrix3d_timepoint_specific = matrix4d_raw_data[
            index_temporal, :, :, :
        ]  # should be 15, 256, 256
        """

        matrix3d_timepoint_specific = matrix4d_raw_data[
            index_temporal, 4:15, :, :
        ]  # should be 11, 256, 256

        matrix3d_timepoint_specific_padded = np.expand_dims(
            matrix3d_timepoint_specific, 0
        )

        specific_timepoint = datetime_increment(
            str2iso(file_starting_date), index_temporal, 3
        )

        matrix2d_predicted_temp_padded = predict_via_CNN_temporal(
            matrix3d_timepoint_specific_padded, input_CNN_model
        )
        matrix2d_predicted_temp = np.squeeze(matrix2d_predicted_temp_padded)

        # 6th variable is the index of the low resolution temperature model
        matrix2d_low_res_air_temp = matrix4d_raw_data[
            index_temporal, 5, :, :
        ]  # 256x256x1

        # 5th variable is the index of the dew temperature
        matrix2d_low_res_dew_temp = matrix4d_raw_data[
            index_temporal, 4, :, :
        ]  # 256x256x1

        ground_truth_temp = matrix4d_label[index_temporal, 0, :, :]

        # Adding them to several lists.
        list_prediction.append(matrix2d_predicted_temp)
        list_dew_temp.append(matrix2d_low_res_dew_temp)
        list_air_temp.append(matrix2d_low_res_air_temp)
        list_truth.append(ground_truth_temp)
        list_timepoint.append(specific_timepoint)

    return list_truth, list_prediction, list_air_temp, list_dew_temp, list_timepoint


def run_ANN_model_on_data(
    input_ANN_model: Sequential,
    filename: str,
    file_starting_date: str = "2018-01-16 12:00:00",
):
    """
    Run a ANN prediction base on the model on the path. This does not deal with CNN model.
    :param input_ANN_model: the input TF flow.
    :param file_path:
    :return:
    """
    list_timepoint = []
    list_prediction = []
    list_dew_temp = []
    list_air_temp = []
    list_truth = []

    # load 4D data for ALL timepoint:
    data_input = f"input_{filename}"
    matrix4d_raw_data = np.load(os.path.join(data_dir, data_input))

    data_label = f"label_{filename}"
    matrix4d_label = np.load(os.path.join(data_dir, data_label))

    # Loop through all timepoint.
    size_temporal_dimension = matrix4d_raw_data.shape[
        0
    ]  # time point is the 1st position.
    for index_temporal in range(size_temporal_dimension):

        matrix3d_timepoint_specific = matrix4d_raw_data[
            index_temporal, :, :, :
        ]  # should be 15, 256, 256

        specific_timepoint = date_increment(file_starting_date, index_temporal)

        matrix2d_predicted_temp = predict_via_ANN(
            specific_timepoint, matrix3d_timepoint_specific, input_ANN_model
        )

        # 6th variable is the index of the low resolution temperature model
        matrix2d_low_res_air_temp = matrix4d_raw_data[
            index_temporal, 5, :, :
        ]  # 256x256x1

        # 5th variable is the index of the dew temperature
        matrix2d_low_res_dew_temp = matrix4d_raw_data[
            index_temporal, 4, :, :
        ]  # 256x256x1

        ground_truth_temp = matrix4d_label[index_temporal, 0, :, :]

        # Adding them to several lists.
        list_prediction.append(matrix2d_predicted_temp)
        list_dew_temp.append(matrix2d_low_res_dew_temp)
        list_air_temp.append(matrix2d_low_res_air_temp)
        list_truth.append(ground_truth_temp)
        list_timepoint.append(specific_timepoint)

    return list_truth, list_prediction, list_air_temp, list_dew_temp, list_timepoint


def predict_via_CNN_temporal(matrix_input, input_CNN_model: Sequential):
    """
    :param input_datetime: date and time component of the current data.
    :param input_matrix:
    :param input_CNN_model: the CNN model where prediction will be run on .
    :return: prediction 2d matrix of temperature based on input data across every single one of the 256 by 256 space.
    """

    # Transpose the matrices axii
    matrix_transposed = matrix_input.transpose([0, 2, 3, 1])

    # predict output.
    prediction_output = input_CNN_model.predict(matrix_transposed)

    return prediction_output


def predict_via_CNN(input_datetime, input_matrix, input_CNN_model: Sequential):
    """
    Take the current date/time (to be collapsed), the input 15 x 256 x 256 matrix and run them against the model to come up with a prediction for EVERY SINGLE ONE of the spatial unitary "pixel". Calls the model 65536 times.
    :param input_datetime: date and time component of the current data.
    :param input_matrix:
    :param input_CNN_model: the CNN model where prediction will be run on .
    :return: prediction 2d matrix of temperature based on input data across every single one of the 256 by 256 space.
    """
    # Generate a 256 by 256 output.
    from generator.Weather_DataSequence5_TwoDimensional import flatten_input

    prediction_input = flatten_input(input_matrix, input_datetime)

    prediction_input_padded = np.vstack([prediction_input])

    # predict output.
    prediction_output = input_CNN_model.predict(prediction_input_padded)

    return prediction_output


def predict_via_ANN(input_datetime, input_matrix, input_model: Sequential):
    """
    Take the current date/time (to be collapsed), the input 15 x 256 x 256 matrix and run them against the model to come up with a prediction for EVERY SINGLE ONE of the spatial unitary "pixel". Calls the model 65536 times.
    :param input_datetime: date and time component of the current data.
    :param input_matrix:
    :param input_model:
    :return: prediction 2d matrix of temperature based on input data across every single one of the 256 by 256 space.
    """
    # Generate a 256 by 256 output.
    prediction_matrix = np.ndarray(shape=(256, 256), dtype=float, order="F")

    current_yearday, current_time = date_split(input_datetime)

    for x_coor in range(256):
        for y_coor in range(256):
            # get input data
            prediction_input = input_matrix[:, x_coor, y_coor]

            # insert placeholder 0s for Y positional information.
            matrix_x_inserted = np.insert(
                prediction_input, 15, values=x_coor
            )  # x coordinate.
            matrix_y_inserted = np.insert(
                matrix_x_inserted, 16, values=y_coor
            )  # y coordinate.
            matrix_yearday_inserted = np.insert(
                matrix_y_inserted, 17, values=current_yearday
            )  # day of the year
            matrix_hour_inserted = np.insert(
                matrix_yearday_inserted, 18, values=current_time
            )  # hour of the day 9 in 3 hours increment.
            # logger.debug(matrix_hour_inserted.shape)

            prediction_input_padded = np.vstack([matrix_hour_inserted])

            # predict output.
            prediction_output = input_model.predict(prediction_input_padded)

            # fill the image matrix.
            prediction_matrix[x_coor, y_coor] = prediction_output

    return prediction_matrix


if __name__ == "__main__":
    pass
