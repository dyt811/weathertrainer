import numpy as np
from matplotlib import pyplot as plt


def visualize(numpy_dataarray: np.ndarray, title: str):
    """
    This is a single panel temperature visualization of RAW weather related data
    :param air_temperature:
    :param dew_temperature:
    :param prediction:
    :param ground_truth:
    :param isodate:
    :return:
    """
    fig, ax1 = plt.subplots(1, 1, figsize=(10, 10))

    # 5 is the index of the temperature among 15 variables.

    ax1.imshow(numpy_dataarray)
    ax1.set_title(title)

    plt.figure()
    plt.show()


def visualize_save(numpy_dataarray: np.ndarray, title: str):
    fig, ax1 = plt.subplots(1, 1, figsize=(10, 10))

    # 5 is the index of the temperature among 15 variables.

    ax1.imshow(numpy_dataarray)
    ax1.set_title(title)

    plt.figure()
    return fig
