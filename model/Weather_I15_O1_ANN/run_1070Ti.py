from model.I15_O1_CNN.FifteenToOneANN import SimpleANN_I15_O1_R


from model.cleanup import cleanLog

# This is the main script entry point to invoke the CNN.

# By importing from different class as model, they can be invoked individually here.

image_shape = (15,)
output_channel = 1

# Model creation:
model1 = SimpleANN_I15_O1_R(image_shape, 1)
model1.create()
model1.compile()
model1.load_data(r"C:\GitHub\MarkerTrainer\data")
model1.run()
