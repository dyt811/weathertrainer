from enum import Enum

class stage(Enum):
     """
     An enum class to keep track of the stage of the current model execution process.
     """
     Initialized = 0
     Created = 1
     Compiled = 2
     DataLoaded = 3
     Ran = 4