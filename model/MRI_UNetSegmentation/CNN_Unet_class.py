import os

from keras.models import Sequential, Model
from keras.layers import (
    Input,
    Dense,
    Conv2D,
    MaxPooling2D,
    Dropout,
    Flatten,
    BatchNormalization,
)
from keras.layers import LeakyReLU, UpSampling2D, concatenate
from keras.callbacks import TensorBoard, ModelCheckpoint
from keras.optimizers import Adam

from PythonUtils.file import unique_name
from PythonUtils.env import load_dotenv_var

from model.abstract import CNN_model
from model.path import get_paths, get_project_root
from model.stage import stage


from generator.csvgen import generate_csv
from generator.Marker_DataSequence_Poses import DataSequence


class unet_2d(CNN_model):

    """
    The sole responsibility of this function is to generate a VERY specific network, SUCH that its create_network function can be easily swapped in via import by the parental function.
    """

    def __init__(self, input_shape, output_classes):
        self.input_shape = input_shape
        self.output_classes = output_classes

        self.train_data = None
        self.validation_data = None
        self.callbacks_list = None

        self.model = None

        self.path_model = None
        self.path_prediction = None
        self.model_stage = stage.Initialized

    def create(self):
        """
        This model is optimizd for REGRESSION type of network no a
        :param input_shape:
        :param output_classes:
        :return:
        """

        # Inspired by https://github.com/zhixuhao/unet/blob/master/model.py

        input_size = (256, 256, 1)

        inputs = Input(input_size)

        conv1 = Conv2D(
            64, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(inputs)
        conv1 = Conv2D(
            64, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(conv1)
        pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

        conv2 = Conv2D(
            128, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(pool1)
        conv2 = Conv2D(
            128, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(conv2)
        pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

        conv3 = Conv2D(
            256, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(pool2)
        conv3 = Conv2D(
            256, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(conv3)
        pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

        conv4 = Conv2D(
            512, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(pool3)
        conv4 = Conv2D(
            512, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(conv4)
        drop4 = Dropout(0.5)(conv4)
        pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

        conv5 = Conv2D(
            1024, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(pool4)
        conv5 = Conv2D(
            1024, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(conv5)
        drop5 = Dropout(0.5)(conv5)

        up6 = Conv2D(
            512, 2, activation="relu", padding="same", kernel_initializer="he_normal"
        )(UpSampling2D(size=(2, 2))(drop5))
        merge6 = concatenate([drop4, up6], axis=3)
        conv6 = Conv2D(
            512, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(merge6)
        conv6 = Conv2D(
            512, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(conv6)

        up7 = Conv2D(
            256, 2, activation="relu", padding="same", kernel_initializer="he_normal"
        )(UpSampling2D(size=(2, 2))(conv6))
        merge7 = concatenate([conv3, up7], axis=3)
        conv7 = Conv2D(
            256, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(merge7)
        conv7 = Conv2D(
            256, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(conv7)

        up8 = Conv2D(
            128, 2, activation="relu", padding="same", kernel_initializer="he_normal"
        )(UpSampling2D(size=(2, 2))(conv7))
        merge8 = concatenate([conv2, up8], axis=3)
        conv8 = Conv2D(
            128, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(merge8)
        conv8 = Conv2D(
            128, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(conv8)

        up9 = Conv2D(
            64, 2, activation="relu", padding="same", kernel_initializer="he_normal"
        )(UpSampling2D(size=(2, 2))(conv8))
        merge9 = concatenate([conv1, up9], axis=3)
        conv9 = Conv2D(
            64, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(merge9)
        conv9 = Conv2D(
            64, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(conv9)
        conv9 = Conv2D(
            2, 3, activation="relu", padding="same", kernel_initializer="he_normal"
        )(conv9)
        conv10 = Conv2D(1, 1, activation="sigmoid")(conv9)

        model = Model(input=inputs, output=conv10)

        model.compile(
            optimizer=Adam(lr=1e-4), loss="binary_crossentropy", metrics=["accuracy"]
        )

        model.summary()

        self.model = model
        self.stage = stage.Created
        return model

    def compile(self):
        """
        This is a model specific compilation process, must choosen/update based on the purpose of the network.
        :param model:
        :return:
        """
        self.model.compile(
            loss="mean_squared_error", optimizer="adadelta", metrics=["acc", "mae"]
        )
        self.stage = stage.Compiled
        return self.model

    def load_data(self):
        """
        Load the data specification from the path in the .env.
        """

        train_path = load_dotenv_var("train_path")
        train_csv_path = load_dotenv_var("train_csv_path")

        validate_path = load_dotenv_var("validate_path")
        validate_csv_path = load_dotenv_var("validate_csv_path")

        # Dynamicly generate model input_path.
        project_root = get_project_root()
        path_log, self.path_model = get_paths(project_root)

        generate_csv(train_path, train_csv_path)
        train_data = DataSequence(train_csv_path, 128, mode="Train")

        generate_csv(validate_path, validate_csv_path)
        validation_data = DataSequence(validate_csv_path, 128)

        model_name = os.path.join(self.path_model, f"{unique_name()}_{__name__}")

        callback_save_model = ModelCheckpoint(
            model_name, monitor="val_acc", verbose=1, save_best_only=True, mode="max"
        )

        # Generate the tensorboard
        callback_tensorboard = TensorBoard(
            log_dir=path_log, histogram_freq=0, write_images=True
        )

        callbacks_list = [callback_tensorboard, callback_save_model]
        self.stage = stage.DataLoaded
        return train_data, validation_data, callbacks_list

    def run(self):
        self.model.fit_generator(
            self.train_data,
            steps_per_epoch=256,
            epochs=500,
            validation_data=self.validation_data,
            validation_steps=256,
            callbacks=self.callbacks_list,
        )
        self.path_prediction = os.path.join(self.path_model, unique_name() + ".h5")
        self.model.save(self.path_prediction)
        self.stage = stage.Ran
        return self.path_prediction
