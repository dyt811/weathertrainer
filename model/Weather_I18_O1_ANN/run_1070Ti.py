from model.I18_O1_CNN.EighteenToOneANN import SimpleANN_I18_O1_R


from model.cleanup import cleanLog

# This is the main script entry point to invoke the CNN.

# By importing from different class as model, they can be invoked individually here.
cleanLog(None)
image_shape = (18,)
output_channel = 1

# Model creation:
model1 = SimpleANN_I18_O1_R(image_shape, 1)
model1.create()
model1.compile()
model1.load_data(r"C:\GitHub\MarkerTrainer\data")
model1.run()
