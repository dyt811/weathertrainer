from keras.models import Sequential
from keras.preprocessing import image
from model.load import model
import os
import numpy as np
from tqdm import tqdm
from PythonUtils.folder import recursive_list
import csv


def prepare_data():
    raise NotImplementedError


def predict(input_model: Sequential, input_image, target_size):

    # predicting multiple images at once
    img = image.load_img(
        input_image,
        target_size=(target_size, target_size),
        color_mode="grayscale",  # comment this line if you see anything like ValueError: Error when checking input: expected conv2d_1_input to have shape (500, 500, 3) but got array with shape (500, 500, 1)
    )
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)

    # pass the list of multiple images np.vstack()
    images = np.vstack([x])

    output = input_model.predict_classes(images)

    """
    predictions = model.predict_generator(self.test_generator)
    predictions = np.argmax(predictions, axis=-1) #multiple categories
    
    label_map = (train_generator.class_indices)
    label_map = dict((v,k) for k,v in label_map.items()) #flip k,v
    predictions = [label_map[k] for k in predictions]
    """
    return output


def folder(input_model: str, input_folder: str, target_size: int):
    """
    Folder version of the prediction function provided above.
    :param input_model:
    :param input_folder:
    :param target_size:
    :return:
    """

    # Load model
    assert os.path.exists(input_model)
    loaded_model = model(input_model)

    # Load files that predictions will be run upon.
    assert os.path.exists(input_folder)
    files = recursive_list(input_folder)

    # Place holder list of the actual prediction results to be returned.
    prediction_list = []

    # Go through all files.
    for file in tqdm(files):
        output = predict(loaded_model, file, target_size)
        # print(output[0])
        prediction_list.append(output[0])

    return prediction_list


def write_classification_prediction(
    model_path: str, input_folder: str, output_csv_path: str
):
    """
    Predict use the model provided on the input data provided. Write to CSV.
    :param model_path:
    :param input_folder: INPUT IMAGES must be TURNED INTO gray scale
    :return:
    """

    list_files = recursive_list(input_folder)
    list_predictions = folder(model_path, input_folder, 500)
    assert len(list_files) == len(list_predictions)

    # Open files for writing.
    with open(output_csv_path, "w", newline="") as csv_file:

        # Open file
        csv_writer = csv.DictWriter(csv_file, fieldnames=["File_Name", "Non-Presence"])
        csv_writer.writeheader()

        for index in range(len(list_files)):

            # Build tuple from the file path and the prediction
            case = {
                "File_Name": list_files[index],
                "Non-Presence": list_predictions[index],  # for classification tasks
            }
            # Write to CSV
            csv_writer.writerow(case)


def write_regression_prediction(
    model_path: str, input_folder: str, output_csv_path: str
):
    """
    Predict use the model provided on the input data provided. Write to CSV.
    :param model_path:
    :param input_folder:
    :return:
    """

    list_files = recursive_list(input_folder)
    list_predictions = folder(model_path, input_folder, 500)
    assert len(list_files) == len(list_predictions)

    # Open files for writing.
    with open(output_csv_path, "w", newline="") as csv_file:

        # Open file
        csv_writer = csv.DictWriter(
            csv_file, fieldnames=["File_Name", "r0", "r1", "r2"]
        )
        csv_writer.writeheader()

        for index in range(len(list_files)):

            # Build tuple from the file path and the prediction
            case = {
                "File_Name": list_files[index],
                "r0": list_predictions[index][0],  # for regression tasks
                "r1": list_predictions[index][1],  # for regression tasks
                "r2": list_predictions[index][2],  # for regression tasks
            }
            # Write to CSV
            csv_writer.writerow(case)


def test_classificaiton():
    write_classification_prediction(
        r"C:\GitHub\MarkerTrainer\models\2018-10-12T21_32_47.624764",
        r"C:\GitHub\MarkerTrainer\data_augmented\bg\2018-10-27T00_51_20.488704500px",
        r"C:\GitHub\MarkerTrainer\results\prediction.csv",
    )


if __name__ == "__main__":
    test_classificaiton()
