import abc  # abstract base class


class CNN_model(abc.ABC):
    """
    This is the abstract class representative of all possible CNN that will be loaded within the framework.
    """

    @abc.abstractmethod
    def create(self):
        pass

    @abc.abstractmethod
    def compile(self):
        pass

    @abc.abstractmethod
    def load_data(self, path: str):
        pass

    def run(self):
        pass
