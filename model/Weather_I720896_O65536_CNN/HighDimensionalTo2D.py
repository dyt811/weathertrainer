import os

from keras.models import Sequential
from keras.layers import (
    Dense,
    Dropout,
    BatchNormalization,
    Conv2D,
    MaxPooling2D,
    Flatten,
    Reshape,
    UpSampling2D,
)
from keras.layers import LeakyReLU
from keras.callbacks import TensorBoard, ModelCheckpoint

from PythonUtils.file import unique_name
from PythonUtils.folder import get_abspath, create

from model.abstract import CNN_model
from model.path import get_paths
from model.stage import stage

from generator.Weather_DataSequence6_SimplifiedInput import DataSequence, ProcessingMode


class CNN_I720896_O65536_R(CNN_model):

    """
    This model is used to conver the 19 dimensional data which include temperature information for a given spatial temporal combination in the weather challenge:
    Input: 19 variable (rough temperature in the low resolution region upsampled to 256 to 256 regions with high resolution 256 by 256 x 14 other weather related measurement.
    Output: 1 Variable (temperature)
    Output Type: Regression
    """

    def __init__(self, input_shape, output_classes):

        self.input_shape = input_shape
        self.output_classes = output_classes

        self.train_data = None
        self.validation_data = None
        self.callbacks_list = None

        self.model = None

        self.path_model = None
        self.path_prediction = None
        self.model_stage = stage.Initialized

    def create(self):
        """
        This model is optimizd for REGRESSION type of network no a
        :param input_shape:
        :param output_classes:
        :return:
        """
        model = Sequential()
        model.add(
            Conv2D(
                16, (3, 3), padding="same", strides=(1, 1), input_shape=self.input_shape
            )
        )
        model.add(BatchNormalization(axis=-1))
        model.add(LeakyReLU(alpha=0.1))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        # model.add(Dropout(0.2))

        model.add(Conv2D(16, (2, 2), padding="same", strides=(1, 1)))
        model.add(BatchNormalization(axis=-1))
        model.add(LeakyReLU(alpha=0.1))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.2))

        model.add(Conv2D(8, (2, 2), padding="same", strides=(1, 1)))
        model.add(LeakyReLU(alpha=0.1))
        model.add(BatchNormalization(axis=-1))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.2))

        model.add(Conv2D(4, (2, 2), padding="same", strides=(1, 1)))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization(axis=-1))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Flatten())

        model.add(Dense(16384))
        model.add(BatchNormalization())
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(0.25))
        model.add(Reshape((128, 128, 1)))

        model.add(UpSampling2D(size=(2, 2)))

        self.model = model
        self.stage = stage.Created

        return model

    def IOCheck(self):
        import numpy as np

        dummy_input = np.ones((11, 256, 256))
        preds = self.model.predict(dummy_input)
        print(preds.shape)

    def compile(self):
        """
        This is a model specific compilation process, must choosen/update based on the purpose of the network.
        :param model:
        :return:
        """
        self.model.compile(
            loss="mse", optimizer="adam", metrics=["mae", "mse", "mape", "cosine"]
        )
        self.model.summary()
        self.stage = stage.Compiled
        return self.model

    def load_data(self, data_path, batch_size=5):
        """
        Load the data specification from the path in the .env.
        """

        # Dynamically generate model input_path.
        this_file = os.path.realpath(__file__)
        project_root = get_abspath(this_file, 2)

        # Log path.
        path_log, self.path_model = get_paths(project_root)

        # Log run path.
        path_log_run = os.path.join(path_log, unique_name() + __name__)

        # Create the Log run path.
        create(path_log_run)

        # Path to the train.
        path_train_spec = r"E:\WeatherData\data\stamped"

        # Generate data sequence.
        train_data = DataSequence(
            path_train_spec, batch_size, mode=ProcessingMode.Train
        )

        # Path to the validation.
        path_validate_spec = r"E:\WeatherData\data\stamped"

        # Generate data sequence.
        validation_data = DataSequence(
            path_validate_spec, batch_size, mode=ProcessingMode.Validation
        )

        # Model name.
        model_name = os.path.join(self.path_model, f"{unique_name()}_{__name__}")

        callback_save_model = ModelCheckpoint(
            model_name,
            monitor="val_mean_absolute_error",
            verbose=1,
            save_best_only=True,
            mode="min",
        )

        # Generate the tensorboard
        callback_tensorboard = TensorBoard(
            log_dir=path_log_run, histogram_freq=0, write_images=True
        )

        self.callbacks_list = [callback_tensorboard, callback_save_model]
        self.stage = stage.DataLoaded
        self.train_data = train_data
        self.validation_data = validation_data

    def run(self, size_step=256, size_epoch=500):
        self.model.fit_generator(
            self.train_data,
            steps_per_epoch=size_step,
            epochs=size_epoch,
            validation_data=self.validation_data,
            validation_steps=size_step,
            callbacks=self.callbacks_list,
        )
        self.path_prediction = os.path.join(self.path_model, unique_name() + ".h5")
        self.model.save(self.path_prediction)
        self.stage = stage.Ran
        return self.path_prediction
