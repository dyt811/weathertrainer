from keras.models import load_model as keras_load_model
from model.abstract import CNN_model


def weight(weight_path, model1: CNN_model):
    raise NotImplementedError
    # Create the model first:
    recreated_model = model1.create()
    # Load the weight.
    recreated_model.load_weight(weiht_path)


def model(model_path):
    """
    Generic model loading function. Load a model from path.
    :param model_path:
    :return:
    """
    loaded_model = keras_load_model(model_path)
    return loaded_model


if __name__ == "__main__":
    model = model(r"C:\GitHub\MarkerTrainer\models\2018-10-12T17_34_56.150751")
