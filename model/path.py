from PythonUtils.folder import get_abspath
import os

def get_project_root():
    """
    Infer the root project folder path location.
    """
    this_file = os.path.realpath(__file__)
    get_abspath(this_file, 1) # The root should be ONE folder level above this one.

def get_paths(project_root_path):
    """
    Based on the project path give, generated and return the other key path necessary for many functions.
    :param project_root_path:
    :return:
    """
    path_log = os.path.join(project_root_path, "logs")
    path_model = os.path.join(project_root_path, "models")
    return path_log, path_model